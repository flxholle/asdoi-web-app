# Asdoi App

An android app for <a href="https://asdoi.gitlab.io">asdoi.gitlab.io</a> based on <a href="https://github.com/varunpvp/android-web-app">varunpvp/android-web-app</a>

[<img src="https://raw.githubusercontent.com/LibreShift/red-moon/master/art/direct-apk-download.png"
      alt="Direct apk download"
      height="130">](https://gitlab.com/asdoi/asdoi-web-app/-/raw/master/app/release/asdoi.apk)  
*Made by <a href="https://github.com/smichel17">smichel17</a> from <a href="https://github.com/LibreShift/red-moon">LibreShift/red-moon</a>
